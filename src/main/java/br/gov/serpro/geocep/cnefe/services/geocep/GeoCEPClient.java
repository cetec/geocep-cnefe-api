package br.gov.serpro.geocep.cnefe.services.geocep;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.util.Encode;

import br.gov.serpro.geocep.cnefe.configuration.GeoCEPConfig;
import br.gov.serpro.geocep.cnefe.persistence.entity.Point;
import br.gov.serpro.geocep.cnefe.services.geocep.exception.NotFoundException;
import br.gov.serpro.geocep.cnefe.services.geocep.exception.ServerErrorException;
import br.gov.serpro.geocep.cnefe.services.geocep.response.BairroResponse;
import br.gov.serpro.geocep.cnefe.services.geocep.response.Localidade;
import br.gov.serpro.geocep.cnefe.services.geocep.response.MunicipioResponse;

/**
 * Cliente para o Serviço GeoCEP Rest.
 * Conecta no serviço e obtém informações Geoespaciais sobre CEPs.
 * 
 * @author CETEC/SERPRO
 */
public class GeoCEPClient {

	@Inject
	private GeoCEPConfig geoCEPConfig;

	/**
	 * Método utilitário que realiza a conexão em um serviço Rest.
	 * 
	 * @param url URL do serviço Rest.
	 * @return String que contém os dados retornados pelo serviço.
	 */
	private String get(final String url) {
		StringBuilder result = new StringBuilder();
		ClientResponse<String> response = null;

		try {
			ClientRequest request = new ClientRequest(url);
			request.accept("application/json");
			request.header("TOKEN", geoCEPConfig.getToken());
			response = request.get(String.class);
		} catch (Exception e) {
			throw new ServerErrorException();
		}

		if (response.getStatus() == 404) {
			throw new NotFoundException();
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(response.getEntity()
				.getBytes())));

		try {
			String output;
			while ((output = br.readLine()) != null) {
				result.append(output);
			}
		} catch (IOException e) {
			throw new ServerErrorException();
		}

		return result.toString();
	}

	/**
	 * Obter uma lista de localidades a partir de uma área informada.
	 * 
	 * @param area Área informada em coordenadas.
	 * @return Lista de localidades encontradas.
	 */
	public List<Localidade> getLocalidadesPorArea(String area) {
		List<Localidade> result = new ArrayList<Localidade>();

		try {
			String response = get(geoCEPConfig.getUrl() + "bairro/area?coordenadas=" + Encode.encodePath(area));
			JSONArray array = new JSONArray(response);
			for (int x = 0; x < array.length(); x++) {
				JSONObject jsonObject = array.getJSONObject(x);
				if (jsonObject.getBoolean("ehMunicipio")) {
					result.add(new MunicipioResponse(jsonObject));
				} else {
					result.add(new BairroResponse(jsonObject));
				}
			}
		} catch (ServerErrorException e) {
			throw e;
		} catch (NotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * Obter uma Localidade (Bairro ou Município) a partir de uma coordenada.
	 * 
	 * @param latitude Coordenada de Latitude.
	 * @param longitude Coordenada de Longitude.
	 */
	public Localidade getLocalidadePorCoordenadas(Double latitude, Double longitude) {
		Localidade result = null;

		try {
			String response = get(geoCEPConfig.getUrl() + "bairro/coordenada/" + latitude + "/" + longitude);
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.getBoolean("ehMunicipio")) {
				result = new MunicipioResponse(jsonObject);
			} else {
				result = new BairroResponse(jsonObject);
			}
		} catch (ServerErrorException e) {
			throw e;
		} catch (NotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * Obter uma Localidade (Bairro ou Município) a partir de um CEP.
	 * 
	 * @param cep CEP.
	 * @return Localidade encontrada.
	 */
	public Localidade getLocalidadePorCEP(String cep) {
		Localidade result = null;

		try {
			String response = get(geoCEPConfig.getUrl() + "bairro/cep/" + cep);
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.getBoolean("ehMunicipio")) {
				result = new MunicipioResponse(jsonObject);
			} else {
				result = new BairroResponse(jsonObject);
			}
		} catch (ServerErrorException e) {
			throw e;
		} catch (NotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * Obter as coordenadas (latitude e longitude) de um CEP.
	 * 
	 * @param cep CEP.
	 * @return Coordenadas.
	 */
	public Point getCoordenadasPorCEP(String cep) {
		Point result = new Point();
		String response = get(geoCEPConfig.getUrl() + "cep/" + cep);

		try {
			JSONObject json = new JSONObject(response);
			result.latitude = json.getDouble("lat");
			result.longitude = json.getDouble("lon");
		} catch (ServerErrorException e) {
			throw e;
		} catch (NotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

}
