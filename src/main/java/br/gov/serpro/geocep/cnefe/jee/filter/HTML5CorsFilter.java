package br.gov.serpro.geocep.cnefe.jee.filter;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

/**
 * Filtro usado para liberar acesso Cross Domain.
 * 
 * @author CETEC/SERPRO
 */
@WebFilter(filterName = "HTML5CorsFilter", urlPatterns = {"/api/*"})
public class HTML5CorsFilter implements javax.servlet.Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) response;
        res.addHeader("Access-Control-Allow-Origin", "*");
        res.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        res.addHeader("Access-Control-Allow-Headers", "Content-Type");
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    	// Ignorado. Nada a fazer.
    }

    @Override
    public void destroy() {
    	// Ignorado. Nada a fazer.
    }

}