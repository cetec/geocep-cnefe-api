package br.gov.serpro.geocep.cnefe.persistence;

import org.postgis.Geometry;
import org.postgis.PGgeometry;
import org.postgis.Point;

public class PostGisUtil {

	/**
	 * Converte um <code>PGgeometry</code> para um <code>Point</code>.
	 * 
	 * @param geom
	 *            o objeto <code>PGgeometry</code>.
	 * @return O objeto <code>Point</code>.
	 */
	public static final Point geomToPoint(final PGgeometry geom) {
		if (geom.getGeoType() == Geometry.POINT) {
			return (Point) geom.getGeometry();
		}

		return null;
	}
	
	/**
	 * Recupera os pontos formatados.
	 * @param coordenadas O array contendo as coordendas.
	 * @return Uma <code>String</code> contendo os pontos formatados.
	 */
	public static final String getPontosFormatadosLineString(double[] coordenadas) {
		if(coordenadas.length < 2) {
			return null;
		}
		
		double x0 = coordenadas[0];
		double y0 = coordenadas[1];
		
		String retorno = "";
		
		for (int i = 0; i < coordenadas.length; i+=2) {
			if(i < coordenadas.length - 2) {
				retorno += coordenadas[i] + " " + coordenadas[i+1] + ",";
			}
		}
		
		// é necessário repetir o primeiro ponto para "fechar" o polígono
		retorno += x0 + " " + y0;
		
		return retorno;
	}
}
