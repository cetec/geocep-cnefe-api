package br.gov.serpro.geocep.cnefe.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;

@SuppressWarnings("serial")
public class Estabelecimento implements Serializable {
	public Point point;
	public BigDecimal codigo;
	public Municipio municipio;
	public String nome;
	public String endereco;
	public int endNumero;
	public String endCompl;
	public String cidade;
	public String uf;
	public String cep;
	public TipoEstabelecimento tipo;

	public Estabelecimento() {
	}

	@Override
	public String toString() {
		return nome;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;

		if (obj instanceof Estabelecimento) {
			Estabelecimento est = (Estabelecimento) obj;
			result = this.codigo.equals(est.codigo);
		}

		return result;
	}

}
