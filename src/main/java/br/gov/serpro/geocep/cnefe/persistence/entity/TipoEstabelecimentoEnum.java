package br.gov.serpro.geocep.cnefe.persistence.entity;

public enum TipoEstabelecimentoEnum {
	DOMICILIO_PARTICULAR(1, "Domicílio Particular"),
	DOMICILIO_COLETIVO(2, "Domicílio Coletivo"),
	ESTABELECIMENTO_AGROPECUARIO(3, "Estabelecimento Agropecuário"),
	ESTABELECIMENTO_ENSINO(4, "Estabelecimento de Ensino"),
	ESTABELECIMENTO_SAUDE(5, "Estabelecimento de Saúde"),
	ESTABELECIMENTO_OUTRAS_FINALIDADES(6, "Estabelecimento de Outras Finalidades"),
	EDIFICACAO_CONSTRUCAO(7, "Edificação em Construção");
	
	private final int codigo;
	private String descricao;

	TipoEstabelecimentoEnum(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public static TipoEstabelecimentoEnum getTipoEstabelecimento(int tipo) {
		switch (tipo) {
		case 1:
			return DOMICILIO_PARTICULAR;
		case 2:
			return DOMICILIO_COLETIVO;
		case 3:
			return ESTABELECIMENTO_AGROPECUARIO;
		case 4:
			return ESTABELECIMENTO_ENSINO;
		case 5:
			return ESTABELECIMENTO_SAUDE;
		case 6:
			return ESTABELECIMENTO_OUTRAS_FINALIDADES;
		case 7:
			return EDIFICACAO_CONSTRUCAO;
		default:
			return null;
		}
	}
}