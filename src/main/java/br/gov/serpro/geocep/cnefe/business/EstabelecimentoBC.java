package br.gov.serpro.geocep.cnefe.business;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import br.gov.serpro.geocep.cnefe.persistence.dao.EstabelecimentoDAO;
import br.gov.serpro.geocep.cnefe.persistence.entity.Estabelecimento;
import br.gov.serpro.geocep.cnefe.persistence.entity.EstabelecimentoLocalidade;
import br.gov.serpro.geocep.cnefe.services.geocep.GeoCEPClient;
import br.gov.serpro.geocep.cnefe.services.geocep.response.Localidade;

@BusinessController
public class EstabelecimentoBC {

	@Inject
	private EstabelecimentoDAO estabelecimentoDAO;

	@Inject
	private GeoCEPClient geoCEPClient;

	public EstabelecimentoLocalidade getEstabelecimentosLocalidadesPorCoordenadas(double latitude, double longitude, int tipo) {
		EstabelecimentoLocalidade result = new EstabelecimentoLocalidade();
		Localidade localidade = geoCEPClient.getLocalidadePorCoordenadas(latitude, longitude);
		result.setEstabelecimentos(estabelecimentoDAO.getEstabelecimentos(tipo, localidade));
		result.addLocalidade(localidade);
		
		return result;
	}

	public EstabelecimentoLocalidade getEstabelecimentosLocalidadesPorArea(String area, int tipo) {
		EstabelecimentoLocalidade result = new EstabelecimentoLocalidade();
		List<Estabelecimento> estabelecimentos = new ArrayList<Estabelecimento>();
		List<Localidade> localidades = geoCEPClient.getLocalidadesPorArea(area);
		
		for (Localidade localidade : localidades) {
			estabelecimentos.addAll(estabelecimentoDAO.getEstabelecimentos(tipo, localidade));
			result.addLocalidade(localidade);
		}
		result.setEstabelecimentos(estabelecimentos);

		return result;
	}

	public EstabelecimentoLocalidade getEstabelecimentosLocalidadesPorCEP(String cep, int tipo) {
		EstabelecimentoLocalidade result = new EstabelecimentoLocalidade();
		Localidade localidade = geoCEPClient.getLocalidadePorCEP(cep);
		result.setEstabelecimentos(estabelecimentoDAO.getEstabelecimentos(tipo, localidade));
		result.addLocalidade(localidade);
		
		return result;
	}

	public Localidade getLocalidadePorCEP(String cep) {
		return geoCEPClient.getLocalidadePorCEP(cep);
	}

	public Localidade getLocalidadePorCoordenadas(Double latitude, Double longitude) {
		return geoCEPClient.getLocalidadePorCoordenadas(latitude, longitude);
	}

	public List<Localidade> getLocalidadesPorArea(String area) {
		return geoCEPClient.getLocalidadesPorArea(area);
	}

}
