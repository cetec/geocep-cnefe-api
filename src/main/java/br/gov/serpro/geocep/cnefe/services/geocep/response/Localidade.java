package br.gov.serpro.geocep.cnefe.services.geocep.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@SuppressWarnings("serial")
public class Localidade extends JSONObject implements Serializable {
	public BigDecimal codigo;
	public String nome;
	public List<String> ceps = new ArrayList<String>();
	public List<Double> sede = new ArrayList<Double>();
	public boolean ehMunicipio;

	public Localidade() {}
	
	public Localidade(JSONObject jsonObject) {
		try {
			codigo = BigDecimal.valueOf(jsonObject.getLong("codigo"));
			nome = jsonObject.getString("nome");

			JSONArray jsonCeps = jsonObject.getJSONArray("ceps");
			for (int x = 0; x < jsonCeps.length(); x++) {
				ceps.add(jsonCeps.getString(x));
			}

			JSONArray jsonSedes = jsonObject.getJSONArray("sede");
			for (int x = 0; x < jsonSedes.length(); x++) {
				sede.add(jsonSedes.getDouble(x));
			}
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

}