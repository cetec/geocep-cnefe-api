package br.gov.serpro.geocep.cnefe.persistence.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import br.gov.frameworkdemoiselle.stereotype.PersistenceController;
import br.gov.serpro.geocep.cnefe.persistence.entity.Estabelecimento;
import br.gov.serpro.geocep.cnefe.persistence.entity.Point;
import br.gov.serpro.geocep.cnefe.persistence.entity.TipoEstabelecimento;
import br.gov.serpro.geocep.cnefe.services.geocep.GeoCEPClient;
import br.gov.serpro.geocep.cnefe.services.geocep.response.Localidade;

/**
 * Controlador de Persistência responsável em obter os dados de Estabelecimentos no banco de dados.
 * A conexão é realizada manualmente através de JDBC.
 * 
 * @author CETEC/SERPRO
 */
@PersistenceController
public class EstabelecimentoDAO {

	@Inject
	private Connection con;

	@Inject
	private MunicipioDAO municipioDAO;

	@Inject
	private GeoCEPClient geoCEP;

	/**
	 * Obter da base de dados todos os estabelecimentos, de um determinado tipo, que estão contidos nas localidades informadas.
	 * 
	 * @param tipo Tipo de Estabelecimento que deve ser retornado.
	 * @param localidades Localidades onde os estabelecimentos estão contidos.
	 * 
	 * @return Lista de Estabelecimentos.
	 */
	public List<Estabelecimento> getEstabelecimentos(int tipo, Localidade localidade) {
		List<Estabelecimento> estabelecimentos = new ArrayList<Estabelecimento>();

		String consultaEstab = "SELECT * FROM cnefe.estab" + " WHERE idc_especie_endereco = '" + tipo + "' AND "
				+ montaCondicaoCEPs(localidade.ceps) + " ORDER BY nom_estabelecimento";

		Statement st = null;
		ResultSet rsEst = null;

		try {
			st = con.createStatement();
			rsEst = st.executeQuery(consultaEstab);
			Estabelecimento estabelecimento = null;

			Map<String, Point> cachePoints = new HashMap<String, Point>();
			while (rsEst.next()) {
				estabelecimento = new Estabelecimento();
				estabelecimento.codigo = rsEst.getBigDecimal("chv_estab");
				estabelecimento.municipio = municipioDAO.getMunicipio(rsEst.getBigDecimal("chv_municipio"));
				estabelecimento.nome = rsEst.getString("nom_estabelecimento");
				estabelecimento.endereco = rsEst.getString("dsc_logradouro");
				estabelecimento.endNumero = Integer.parseInt(rsEst.getString("dsc_numero_logradouro"));
				estabelecimento.endCompl = rsEst.getString("dsc_complemento") + " " + rsEst.getString("nom_localidade");
				estabelecimento.cep = rsEst.getString("num_cep");
				estabelecimento.tipo = new TipoEstabelecimento(rsEst.getInt("idc_especie_endereco"));

				if (cachePoints.containsKey(estabelecimento.cep)) {
					estabelecimento.point = cachePoints.get(estabelecimento.cep);
				} else {
					estabelecimento.point = geoCEP.getCoordenadasPorCEP(estabelecimento.cep);
					cachePoints.put(estabelecimento.cep, estabelecimento.point);
				}

				estabelecimentos.add(estabelecimento);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (rsEst != null && st != null) {
					rsEst.close();
					st.close();
				}
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}

		return estabelecimentos;
	}

	/**
	 * Monta a condição da clausula WHERE para a coluna CEP da tabela.
	 * 
	 * @param ceps As faixas de CEPs a serem pesquisadas.
	 * @return Uma <code>String</code> que representa a condição da consulta.
	 */
	private String montaCondicaoCEPs(List<String> ceps) {
		StringBuilder result = new StringBuilder();

		if (ceps.size() == 1) {
			result.append("(num_cep = " + ceps.iterator().next() + ")");
		} else {
			result.append("(");

			if (ceps.size() % 2 == 0) {
				for (int i = 0; i < ceps.size(); i += 2) {
					result.append("(num_cep >= '");
					result.append(fillCEP(ceps.get(i)));
					result.append("' AND num_cep <= '");
					result.append(fillCEP(ceps.get(i + 1)) + "')");

					if (i < ceps.size() - 2) {
						result.append(" OR ");
					}
				}
			}
			
			result.append(")");
		}

		return result.toString();
	}

	/**
	 * Preencher o restante do CEP com 0 (zeros).
	 * 
	 * @param cep CEP a ser preenchido.
	 * @return CEP preenchido com 0s.
	 */
	private String fillCEP(final String cep) {
		StringBuilder result = new StringBuilder();

		for (int x = cep.length(); x < 8; x++) {
			result.append("0");
		}
		result.append(cep);

		return result.toString();
	}

}
