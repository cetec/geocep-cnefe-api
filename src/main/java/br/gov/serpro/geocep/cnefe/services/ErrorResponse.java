package br.gov.serpro.geocep.cnefe.services;

public class ErrorResponse {
	public String mensagem;
	public int codigo;

	public ErrorResponse() {
	}

	public ErrorResponse(String mensagem, int codigo) {
		this.mensagem = mensagem;
		this.codigo = codigo;
	}

}
