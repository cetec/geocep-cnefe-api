package br.gov.serpro.geocep.cnefe;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class Api extends Application {

}