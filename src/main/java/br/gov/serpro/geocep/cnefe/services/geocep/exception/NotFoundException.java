package br.gov.serpro.geocep.cnefe.services.geocep.exception;

public class NotFoundException extends RuntimeException {
	private static final long serialVersionUID = -3400980180449459399L;
}
