package br.gov.serpro.geocep.cnefe.services;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import br.gov.serpro.geocep.cnefe.business.EstabelecimentoBC;
import br.gov.serpro.geocep.cnefe.persistence.entity.TipoEstabelecimento;
import br.gov.serpro.geocep.cnefe.persistence.entity.TipoEstabelecimentoEnum;
import br.gov.serpro.geocep.cnefe.services.geocep.exception.NotFoundException;

@Path("estabelecimentos")
@Produces(APPLICATION_JSON)
public class EstabelecimentoService {

	@Inject
	private EstabelecimentoBC estabelecimentoBC;

	@GET
	@Path("tipos")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public List<TipoEstabelecimento> getTiposEstabelecimentos() throws Exception {
		List<TipoEstabelecimento> tipos = new ArrayList<TipoEstabelecimento>();

		tipos.add(new TipoEstabelecimento(TipoEstabelecimentoEnum.DOMICILIO_PARTICULAR));
		tipos.add(new TipoEstabelecimento(TipoEstabelecimentoEnum.DOMICILIO_COLETIVO));
		tipos.add(new TipoEstabelecimento(TipoEstabelecimentoEnum.EDIFICACAO_CONSTRUCAO));
		tipos.add(new TipoEstabelecimento(TipoEstabelecimentoEnum.ESTABELECIMENTO_AGROPECUARIO));
		tipos.add(new TipoEstabelecimento(TipoEstabelecimentoEnum.ESTABELECIMENTO_ENSINO));
		tipos.add(new TipoEstabelecimento(TipoEstabelecimentoEnum.ESTABELECIMENTO_OUTRAS_FINALIDADES));
		tipos.add(new TipoEstabelecimento(TipoEstabelecimentoEnum.ESTABELECIMENTO_SAUDE));

		return tipos;
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getEstabelecimentos(@QueryParam("tipo") Integer tipo, @QueryParam("latitude") Double latitude,
			@QueryParam("longitude") Double longitude, @QueryParam("area") String area, @QueryParam("cep") String cep) {

		Response result = null;

		if (area != null && !"0".equals(area)) {
			result = getEstabelecimentosPorArea(tipo, area);
		} else if (cep != null && !"0".equals(cep)) {
			result = getEstabelecimentosPorCEP(tipo, cep);
		} else {
			result = getEstabelecimentosPorCoordenada(tipo, latitude, longitude);
		}

		return result;
	}

	/**
	 * Obter todos os estabelecimentos e localidades a partir de um CEP.
	 * 
	 * @param tipo Tipo.
	 * @param cep CEP.
	 * @return Lista de estabelecimentos e localidades.
	 */
	private Response getEstabelecimentosPorCEP(Integer tipo, String cep) {
		ResponseBuilder result = null;

		try {
			result = Response.status(Status.OK).entity(
					estabelecimentoBC.getEstabelecimentosLocalidadesPorCEP(cep, tipo));
		} catch (NotFoundException e) {
			result = Response.status(Status.NOT_FOUND).entity(
					new ErrorResponse("Não existem estabelecimentos para o CEP informado.", 404));
		} catch (Exception e) {
			result = Response.status(Status.SERVICE_UNAVAILABLE).entity(
					new ErrorResponse("Ocorreu um erro no servidor.", 501));
		}

		return result.build();
	}

	/**
	 * Obter todos os estabelecimentos e localidades a partir de uma coordenada e tipo de estabelecimento.
	 * 
	 * @param tipo Tipo.
	 * @param latitude Latitude.
	 * @param longitude Longitude.
	 * @return Lista de estabelecimentos e localidades.
	 */
	private Response getEstabelecimentosPorCoordenada(Integer tipo, Double latitude, Double longitude) {
		ResponseBuilder result = null;

		try {
			result = Response.status(Status.OK).entity(
					estabelecimentoBC.getEstabelecimentosLocalidadesPorCoordenadas(latitude, longitude, tipo));
		} catch (NotFoundException e) {
			result = Response.status(Status.NOT_FOUND).entity(
					new ErrorResponse("Não existem estabelecimentos para a coordenada informada.", 404));
		} catch (Exception e) {
			result = Response.status(Status.SERVICE_UNAVAILABLE).entity(
					new ErrorResponse("Ocorreu um erro no servidor.", 501));
		}

		return result.build();
	}

	/**
	 * Obter todos os estabelecimentos e localidades a partir de uma área e um tipo de estabelecimento.
	 * 
	 * @param tipo Tipo.
	 * @param area Área.
	 * @return Lista de estabelecimentos e localidades.
	 */
	private Response getEstabelecimentosPorArea(Integer tipo, String area) {
		ResponseBuilder result = null;

		try {
			result = Response.status(Status.OK).entity(
					estabelecimentoBC.getEstabelecimentosLocalidadesPorArea(area, tipo));
		} catch (NotFoundException e) {
			result = Response.status(Status.NOT_FOUND).entity(
					new ErrorResponse("Não existem estabelecimentos para a área informada.", 404));
		} catch (Exception e) {
			result = Response.status(Status.SERVICE_UNAVAILABLE).entity(
					new ErrorResponse("Ocorreu um erro no servidor.", 501));
		}

		return result.build();
	}

}
