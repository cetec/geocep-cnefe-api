package br.gov.serpro.geocep.cnefe.persistence.entity;

import java.io.Serializable;

public class TipoEstabelecimento implements Serializable {

	private static final long serialVersionUID = -2055017462801347370L;
	
	public int codigo;
	public String descricao;

	public TipoEstabelecimento() {
	}
	
	public TipoEstabelecimento(int codigo) {
		this.codigo = codigo;
		descricao = TipoEstabelecimentoEnum.getTipoEstabelecimento(codigo).getDescricao();
	}
	
	public TipoEstabelecimento(TipoEstabelecimentoEnum tipo) {
		this.codigo = tipo.getCodigo();
		descricao = tipo.getDescricao();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TipoEstabelecimento) {
			TipoEstabelecimento est = (TipoEstabelecimento) obj;

			return this.codigo == est.codigo;
		}

		return false;
	}
	
}
