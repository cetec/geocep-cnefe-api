package br.gov.serpro.geocep.cnefe.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class Municipio implements Serializable {

	private static final long serialVersionUID = -6900414831889927939L;
	
	public BigDecimal codigo;
	public String nome;
	public String uf;

	public Municipio() {
	}
	
	@Override
	public String toString() {
		return nome;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Municipio) {
			Municipio mun = (Municipio) obj;

			return this.codigo.equals(mun.codigo);
		}

		return false;
	}
}
