package br.gov.serpro.geocep.cnefe.persistence.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Point implements Serializable {
	public Double latitude;
	public Double longitude;

	public Point() {
	}

	public Point(Double latitude, Double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;

		if (obj != null && obj instanceof Point) {
			if (latitude != null && longitude != null) {
				Point other = (Point) obj;
				result = latitude.equals(other.latitude) && longitude.equals(other.longitude);
			}
		}

		return result;
	}

}
