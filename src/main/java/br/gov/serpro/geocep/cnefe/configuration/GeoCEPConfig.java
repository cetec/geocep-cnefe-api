package br.gov.serpro.geocep.cnefe.configuration;

import br.gov.frameworkdemoiselle.configuration.Configuration;

@Configuration(resource = "geocep")
public class GeoCEPConfig {
	private String token;
	private String url;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
