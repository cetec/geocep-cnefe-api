package br.gov.serpro.geocep.cnefe.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.slf4j.Logger;

public class ConnectionFactory {

	@Inject
	private Logger logger;

	/**
	 * Recupera ou cria a conexão com o BD.
	 * 
	 * @return Uma <code>Connection</code> representando a conexão com o BD.
	 * @throws InstantiationException Se não for possível instanciar a conexão.
	 * @throws IllegalAccessException Se não por permitido o acesso à conexão.
	 * @throws ClassNotFoundException Se não for achado, no classpath, as classes instanciadas no método.
	 * @throws SQLException Se houver algum problema na conexão com o BD.
	 */
	@Produces
	@RequestScoped
	public Connection getConnection() throws SQLException, InstantiationException, IllegalAccessException,
			ClassNotFoundException {

		logger.debug("Abrindo a conexão.");

		Class.forName("org.postgresql.Driver").newInstance();
		Connection connection = DriverManager.getConnection("jdbc:postgresql://161.148.20.42:5432/geocep_carga",
				"geocep", "geocep");

		return connection;
	}

	public void close(@Disposes Connection cnn) throws SQLException {
		logger.debug("Fechando a conexão.");
		cnn.close();
	}

}