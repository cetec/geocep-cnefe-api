package br.gov.serpro.geocep.cnefe.services.geocep.response;

import java.io.Serializable;

import org.codehaus.jettison.json.JSONObject;

@SuppressWarnings("serial")
public class CepResponse extends JSONObject implements Serializable {
	public int cep;
	public double lat;
	public double lon;

	public CepResponse() {
	}

}
