package br.gov.serpro.geocep.cnefe.persistence.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.inject.Inject;

import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import br.gov.serpro.geocep.cnefe.persistence.entity.Municipio;

@BusinessController
public class MunicipioDAO {
	
	@Inject
	private Connection con;
	
	/**
	 * Recurepa o município especificado.
	 * @param codMunicipio O código do município.
	 * @return O <code>Municipio</code> respectivo ao código, ou <code>null</code> caso não exista no BD.
	 * @throws SQLException Lançada casso ocorra algum problem na conexão com banco.
	 */
	public Municipio getMunicipio(BigDecimal codigo)
			throws SQLException {
		String consulta = "SELECT * FROM cnefe.municipio WHERE chv_municipio = " + codigo;

		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(consulta);
		
		Municipio municipio = null;
		
		if(rs.next()) {
			municipio = new Municipio();
			municipio.codigo = rs.getBigDecimal("chv_municipio");
			municipio.nome = rs.getString("nom_municipio");
			municipio.uf = rs.getString("sig_uf");
		}
		
		rs.close();
		st.close();
		
		return municipio;
	}
}
