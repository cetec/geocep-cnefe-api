package br.gov.serpro.geocep.cnefe.persistence.entity;

import java.util.ArrayList;
import java.util.List;

import br.gov.serpro.geocep.cnefe.services.geocep.response.Localidade;

/**
 * Entidade que agrega as informações entre estabelecimentos e localidades.
 * 
 * As duas informações estão sempre relacionadas, pois os estabelecimentos
 * estão contidos nas localidades. Ou as localidades contém os estabelecimentos.
 * 
 * Entretanto, não há nada que informe diretamente para qual localidade um 
 * estabelecimento está relacionado.
 * 
 * @TODO Verificar se não é melhor refatorar e incluir os estabelecimentos como
 * um atributo da Localidade. Verificar se não ocorrerá ambiguidades, repetições de entidades
 * ou se vai tornar o processamento no lado cliente mais oneroso.
 */
public class EstabelecimentoLocalidade {
	private List<Estabelecimento> estabelecimentos;
	private List<Localidade> localidades = new ArrayList<Localidade>();
	
	public EstabelecimentoLocalidade() {
	}

	public EstabelecimentoLocalidade(List<Estabelecimento> estabelecimentos, List<Localidade> localidades) {
		this.setEstabelecimentos(estabelecimentos);
		this.setLocalidades(localidades);
	}
	
	public void addLocalidade(Localidade localidade) {
		getLocalidades().add(localidade);
	}

	public List<Estabelecimento> getEstabelecimentos() {
		return estabelecimentos;
	}

	public void setEstabelecimentos(List<Estabelecimento> estabelecimentos) {
		this.estabelecimentos = estabelecimentos;
	}

	public List<Localidade> getLocalidades() {
		return localidades;
	}

	public void setLocalidades(List<Localidade> localidades) {
		this.localidades = localidades;
	}
}
