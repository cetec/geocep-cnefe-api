package br.gov.serpro.geocep.cnefe.services.geocep.response;

import java.io.Serializable;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@SuppressWarnings("serial")
public class MunicipioResponse extends Localidade implements Serializable {
	public String uf;

	public MunicipioResponse() {
	}
	
	public MunicipioResponse(JSONObject jsonObject) {
		super(jsonObject);
		try {
			uf = jsonObject.getString("uf");
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

}
