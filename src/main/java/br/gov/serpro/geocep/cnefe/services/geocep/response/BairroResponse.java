package br.gov.serpro.geocep.cnefe.services.geocep.response;

import java.io.Serializable;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@SuppressWarnings("serial")
public class BairroResponse extends Localidade implements Serializable {
	public MunicipioResponse municipio;

	public BairroResponse() {
	}

	public BairroResponse(JSONObject jsonObject) {
		super(jsonObject);
		try {
			super.ehMunicipio = jsonObject.getBoolean("ehMunicipio");
			if (super.ehMunicipio) {
				municipio = new MunicipioResponse(jsonObject.getJSONObject("municipio"));
			}
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}

	}

}
